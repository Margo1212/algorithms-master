const fs = require("fs");
const path = require("path");

(function init() {
  const users = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/users.json"), "utf-8"));
  const mobileDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/mobile_devices.json"), "utf-8"));
  const iotDevices = JSON.parse(fs.readFileSync(path.resolve(__dirname, "../data/iot_devices.json"), "utf-8"));

  console.log(new Date().toISOString());
  console.log(count(users, mobileDevices, iotDevices));
  console.log(new Date().toISOString());
})();

function count(users, mobileDevices, iotDevices) {
  const result = []
  for (let item of mobileDevices) {
    let sumOfDevices = 0;
    let username = '';
    for (let iotDevise of iotDevices) {
      if (item.id === iotDevise.mobile) {
        sumOfDevices += 1;
      }
      for (let user of users) {
        if (item.user === user.id) {
            username = JSON.stringify(user.name).replace(/[0-9/-/" "/-]/g, '');
        } 
      }     
    }
  result.push({name: username, sum: sumOfDevices})  
}

return  result.reduce((res, value) => {
  if (!res[value.name]) {
      res[value.name] = {
          numberOfDevices: 0
      }
  }
  res[value.name].numberOfDevices += value.sum
  return res
 }, {});
}
  
  